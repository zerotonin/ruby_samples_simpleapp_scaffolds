class User < ActiveRecord::Base
	has_many :friendships, :foreign_key => "user_id", :dependent => :destroy
	has_many :occurances_as_friend, :class_name => "Friendship", :foreign_key => "friend_id", :dependent => :destroy
end
